# Main website

Using Gitlab Pages

## Architecture

```bash
.
├── README.md
├── package-lock.json
├── package.json
├── scss              # SCSS Folder
└── static            # Website folder
    ├── assets        # Static assets
    ├── css           # Static CSS
    ├── favicon.ico   # Favicon
    ├── favicon.png
    ├── fonts         # Fonts     
    ├── index.html    # Index HTML
    └── js            # JS sources
```


