const fs = require("fs");
const path = require("path");

//# /mnt/c/Users/adamd/Downloads/libwebp-1.3.0-linux-x86-64/bin/cwebp assets/models/walker/visage.jpg -m 6  -resize 0 1400 -o assets/models/walker/visage.webp

const output = path.join(__dirname);

function getModelViewer(name) {
    let num = name.match(/^\d+/)[0];
    let paul = name.match(/^\d+-(.+)/)[1];
    return `
    <article>
        <model-viewer alt="${paul}" src="assets/models/${name}/base.gltf" camera-orbit="30deg 90deg 10m"
        environment-image="assets/comfy_cafe_1k.hdr" exposure="1.3" shadow-intensity="2" camera-controls>
        </model-viewer>
        <div class="title">
            <h2>${paul}</h2>
            <p class="subtitle">Paul #${num}</p>
        </div>
    </article>
    `
}


const texturePath = path.join(__dirname, "assets", "textures");
const files = fs.readdirSync(texturePath, {withFileTypes: true});
let models  = []
for (let file of files) {
    let [_, type] = file.name.split(/\.*.*\./);
    let nameWithoutType = file.name.match(/^(.+)\.[^\.]+/)[1];
    const output_models = path.join(output, "assets", "models", nameWithoutType);

    fs.rmSync(output_models, {recursive: true, force: true});
    fs.mkdirSync(output_models, {recursive: true});
    fs.linkSync(path.join(__dirname, "assets", "models", "base", "base.gltf"), path.join(output_models, "base.gltf"));
    fs.linkSync(path.join(file.path, file.name), path.join(output_models, "visage." + type));
    models.push(nameWithoutType);
}

const texturePathv2 = path.join(__dirname, "assets", "textures-v2");
const filesv2 = fs.readdirSync(texturePathv2, {withFileTypes: true});

for (let file of filesv2) {
    let [_, type] = file.name.split(/\.*.*\./);
    let nameWithoutType = file.name.match(/^(.+)\.[^\.]+/)[1];
    const output_models = path.join(output, "assets", "models", nameWithoutType);

    fs.rmSync(output_models, {recursive: true, force: true});
    fs.mkdirSync(output_models, {recursive: true});
    fs.linkSync(path.join(__dirname, "assets", "models", "base-v2", "base.gltf"), path.join(output_models, "base.gltf"));
    fs.linkSync(path.join(file.path, file.name), path.join(output_models, "visage." + type));
    models.push(nameWithoutType);
}
models.push("032-pairet");
let modelViewers = models.sort().reduce((a, b) => a + getModelViewer(b), "");

const destination = path.join(output, "index.html");
let content = fs.readFileSync(path.join(__dirname, "index.template.html"), {encoding: 'utf-8'});
content = content.replace("$_MODEL_$", modelViewers)
fs.writeFileSync(destination, content)
