class Commands {

  IGNORED_FUNCTIONS = ["getPrompt", "constructor", "execute", "notfound", "constructor", "autoComplete", "getMethods"];
  BINARY_DATA = "&lt;binary data&gt;"

  constructor() {
    let init_fs = {
      "home" : {
        "adam" : {
          "cv.json": "url://assets/json/portfolio.json",
          "index.html": "url://index.html",
        },
        "guest": {}
      },
      "etc": {},
      "boot": {"initrd.img-5.15.0-41-generic": this.BINARY_DATA, "vmlinuz-5.15.0-40-generic": this.BINARY_DATA},
      "bin" : this.getMethods().reduce( (p, a) => Object.assign(p, {[a]: this[a].toString()}), {}),
    }
    function initFS(fs_config, current_fs = new Folder("")) {
      for (let key in fs_config) {
        if (typeof fs_config[key] == "object") {
          let new_folder = new Folder(key);
          current_fs.addFile(new_folder);
          initFS(fs_config[key], new_folder);
        } else if (typeof fs_config[key] == "string") {
          if (fs_config[key].startsWith("url:")) {
            let file = new WebFile(key, fs_config[key].split("url://").pop());
            current_fs.addFile(file);
          } else {
            let file = new File(key, fs_config[key]);
            current_fs.addFile(file);
          }
        }
      }
      return current_fs;
    }
    this.user = "adam"
    this.fs = initFS(init_fs);
    this.current_dir = this.fs.findPath(["home","adam"]);
    this.prompt = this.getPrompt();
  }

  async execute(line, history, vue_object) {
    console.log("execute", line, history);
    let splitter = /(?: |\n|^)("(?:(?:"")*[^"]*)*"|[^" \n]*|(?:\n|$))/g;
    let splitted_line = [...line.matchAll(splitter)].map(b=>b[1].replace(/^"|"$/g, ''));
    let command = splitted_line.shift();
    command = command.toLowerCase();

    let named_parameters = {"_": []};
    let last_named_parameter = null;
    for(let i = 0; i < splitted_line.length; i++) {
      if (splitted_line[i].startsWith("-")) {
        let named_parameter = splitted_line[i].replace(/^-+/, '');
        named_parameters[named_parameter] = true;
        last_named_parameter = named_parameter;
      } else if (last_named_parameter) {
        named_parameters[last_named_parameter] = splitted_line[i]
        last_named_parameter = null;
      } else {
        named_parameters["_"].push(splitted_line[i])
      }
    }
    console.log("named_parameters", named_parameters);

    if (typeof this[command] == "function") {
      history.push({prompt: this.getPrompt(), command: line, result: await this[command](vue_object, splitted_line, named_parameters) || ""})
    } else {
      history.push({prompt: this.getPrompt(), command: line, result: this.notfound(command)})
    }
  }
  
  notfound(line) {
    return 'sh: command not found: ' + line;
  }

  getMethods(){
    return Object
      .getOwnPropertyNames(Object.getPrototypeOf(this))
      .filter(item => typeof this[item] === 'function')
      .filter(item => !this.IGNORED_FUNCTIONS.includes(item) && !item.match(/_help/) && !item.startsWith("_"))
      .sort()
  }
  // ################# SHELL EMULATION #################

  autoComplete(prefix_command, current_text, vue_object) {
    let commands = [];
    if (prefix_command.split(" ").length > 1) {
      let precommand = prefix_command.split(" ");
      let end_of = precommand.splice(1);
      if (end_of.length == 0 || end_of.length > 1) {
        return prefix_command;
      }
      let path = end_of[0].split("/");
      let end_path = path.splice(-1);
      let dir = this.current_dir.findPath([...path]);
      commands = Object
        .entries(dir.files)
        .filter(a => a[0].startsWith(end_path))
        .map(a => {
          let b = [...path]
          b.push(a[0])
          return `${precommand} ${b.join("/")}`
        });
    } else {
      commands = this.getMethods()
        .filter(item => item.startsWith(prefix_command))
    }
    let index = commands.findIndex(item => item == current_text);
    index++;
    if (index >= commands.length) return prefix_command;
    return commands[index];
  }

  getPrompt() {
    let pwd = this.pwd();
    if (pwd == "/home/"+this.user) pwd = "~"
    if (pwd == "") pwd = "/"
    pwd = pwd.replace(/\//g, '&#47;')
    return `<span class="username">${this.user}</span> in <span class="folder">${pwd} </span>`;
  }

  pwd_help() {
    return "Usage: pwd\nDiplay current directory"
  }

  pwd() {
    return this.current_dir.pwd().join("/");
  }

  su_help() {
    return "Usage: su [USER]\nChange user"
  }

  su(vue_object, arg) {
    if (arg.length == 0) {return "Error: too few arguments.\nRead the help with \nhelp su"}
    if (arg.length == 1) {
      this.user = arg[0];
      this.prompt = this.getPrompt();
      vue_object.prompt = this.prompt;
      return
    }
    return "Error: too many arguments";
  }

  ls_help() {
    return "Usage: ls [FOLDER]\nList files"
  }

  ls(_, arg) {
    if (arg.length == 0) {
      return Object.keys(this.current_dir.files).join(" ");
    }
    else if (arg.length == 1) {
      try {
        let file = this.current_dir.findPath(arg[0].split("/"))
        if (file instanceof Folder) return Object.keys(file.files).join(" ");
        else return file.name;
      } catch (error) {
        return error.message;
      }
    }
    return `Error: Too many arguments`;
  }

  mkdir_help() {
    return "Usage: mkdir [FOLDER]\nMake directory"
  }

  mkdir(_, arg) {
    if (arg.length == 0) {return "Error: too few arguments.\nRead the help with \nhelp mkdir"}
    else {
      for (let folder_name in arg) {
        let folder = new Folder(arg[folder_name]);
        this.current_dir.addFile(folder);
      }
    }
  }

  echo_help() { return "Usage: echo [STRING]...\nDisplay a line of text"}

  echo(_, arg) {
    return arg.join(" ");
  }

  cat_help() { return "Usage: cat [FILE]...\nConcatenate files and print on the standard output"}

  async cat(_, arg) {
    let res = ""
    for(let file of arg) {
      try {
        let ffile = this.current_dir.findPath(file.split("/"));
        console.log(ffile);
        res += await ffile.getValue() + "\n";
      } catch (error) {
        res += error.message + "\n";
      }
    }
    console.log(res)
    return res;
  }

  cd_help() { return "Usage: cd [FOLDER]\nChange Directory"}

  cd(vue_object, arg) {
    if(arg.length == 0 || (arg.length == 1 && arg[0] == "~")) {
      return this.cd(vue_object, ["/home/"+this.user]);
    } else if (arg.length == 1) {
      let path = arg[0].split("/")
      try {
        let next_path = this.current_dir.findPath(path);
        if (next_path && next_path instanceof Folder) {
          this.current_dir = next_path;
        } else {
          return "Error cannot cd a file";
        }
        vue_object.prompt = this.getPrompt();
        return ""
      } catch (error) {
        console.error(error)
        return error.message;
      }
    } else {
      return "Error: Too many arguments passed"
    }
  }

  help(_, arg) {
    if (arg== null || typeof arg == "undefined" || arg.length == 0) {
      let base_message = `Usage: help [COMMAND]
      List all available commands.\n\n`;
      let properties = this.getMethods()
      
      return base_message + '<div class="flex">' + properties.reduce((a, b) => `${a}<span>${b}</span>`, "") + "</div>";
    } else if (typeof arg[0] == 'string' && typeof this[arg[0]+"_help"] == 'function') {
      return this[arg[0]+"_help"]()
    }
    return `No manual entry for ${arg[0]}`;
    
  }
  // ################# SHELL EMULATION #################
  // ################# SNAKE #################
  async snake(vue_object, arg) {
    await require("js/snake.js");
    vue_object.interactive = false;
    new Snake(vue_object, parseInt(arg[0]) || 10);
    return "Loading..."
  }

  snake_help() {
    return "Usage: snake [SIZE]\nPlay this new game called snake."
  }
  // ################# SNAKE #################
  // ################# MOVIES COMMANDS #################
  movies(vue_object, arg, named_parameters) {
    if (!this.movies_commands) {
      this.movies_commands = new Promise(async (resolve) => {
        await require("js/dist/sql-wasm.js");
        await require("js/movies_commands.js");
        resolve(new MoviesCommands(vue_object));
      });
    }
    vue_object.interactive = false;
    this.movies_commands.then(a => a.run(arg, named_parameters));
    return "Loading...";
  }

  movies_help() {
    return  `Usage:
    - movies list [--page PAGE] [--order COLUMN] [--asc|--desc] : List all movie I have seen
    - movies get [TITLE] [--page PAGE] [--order COLUMN] [--asc|--desc] : List all movie I have seen with TITLE with their rating`;
  }
  // ################# MOVIES COMMANDS #################
  // ################# EXPO COMMANDS #################

  expos = {
    "paul-meridien": {
      link: "/paul-meridien/",
      description: "Le dessin de Paul pendant la pause méridienne"
    }
  }

  expo(_, arg) {
    if (arg in this.expos) {
      return this._redirect(this.expos[arg].link)
    }

    return this.expo_help()
  }

  expo_help() {
    let h = `Usage:
    - expo [NAME] : view the exposition

available expositions :`
    for (let n in this.expos) {
      h += `\n    - ${n} : ${this.expos[n].description}`
    }
    return h
  }
  // ################# EXPO COMMANDS #################
  // ################# COOL TEXT #################
  cooltext(_, text) {
    return figlet.textSync(text.join(" "), 'Slant')
  }

  cooltext_help() {
    return "Usage: cooltext text\nReturn text coolyfied. Test it"
  }
  // ################# COOL TEXT #################
  // ################# LINKS #################

  gitlab() {
    return '<a href="https://gitlab.com/Cooq" target="_blank" >See some awesome projects and dead ones</a>'
  }
  gitlab_help() {
    return "Usage: gitlab\nReturn the link you asked."
  }
  
  linkedin() {
    return '<a href="https://www.linkedin.com/in/adam-coquery/" target="_blank" >You asked for it</a>'
  }
  linkedin_help() {
    return "Usage: linkedin\nReturn the link you asked to know more about me."
  }

  // ################# LINKS #################


  _redirect(url) {
    return `<meta http-equiv="Refresh" content="0; url='${url}'" />`
  }

}

class File {
  constructor(name, value=null, format="text") {
    this.name = name;
    this.format = format;
    this.value = this.formatValue(value);
    this.parent = null;
  }
  getValue() {
    return this.value;
  }
  formatValue(val) {
    if (!val) return val;
    val = val
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;')
      .replace(/\//g, '&#47;')
      .replace(/=/g, '&#61;')
      // .replace(/"/g, '&#34;');
    console.log(this.format)
    if (this.format == "json") {
      val = "<span class='json'>"+ val
        .replace(/("[^"]+"):/g, "<span class='key'>$1</span>:")
        .replace(/:(\s*"[^"]+")/g, ":<span class='value'>$1</span>")
        .replace(/([{}])/g, "<span class='acc'>$1</span>")
        .replace(/([\[\]])/g, "<span class='bracket'>$1</span>")
        + "</span>"
    } else if (this.format == "html") {
      val = "<span class='html'>"+ val
        .replace(/(&lt;!?|&lt;\s*&#47;)(\s*\w+)/g, "$1<span class='balise name'>$2</span>")
        .replace(/([\w@\.-\s]+)&#61;(\s*"[^"]+")/g, "<span class='balise key'>$1</span>&#61;<span class='balise value'>$2</span>")
        + "</span>"
    }
    return val;
  }
  findPath(path) {
    console.log(this.name, path);
    if (path.length == 0) {
      return this;
    } else {
      throw new Error("Error: Path not found " + path.join('/'));
    }
  }
}

class WebFile extends File {
  constructor(name, url) {
    super(name);
    this.url = url;
    this.format = url.split(".").pop();
    console.log(this.format)
  }
  async getValue() {
    if (!this.value) {
      this.value = await (await fetch(this.url)).text();
      this.value = this.formatValue(this.value);
    }
    return this.value;
  }
}

class Folder extends File {
  constructor(name, files = {}) {
    super(name);
    this.files = files;
  }
  getValue() {
    throw new Error("Error: No value for folder");
  }
  addFile(file) {
    this.files[file.name] = file; 
    file.parent = this;
  }
  removeFile(file) {
    delete this.files[file.name];
  }
  findPath(path) {
    // console.log(this.name, path);
    if (path.length === 0 || (path[0] == this.name && path.length == 1)) {
      return this
    }
    if (path[0] === '' && this.parent !== null) {
      return this.parent.findPath(path);
    } else if (path[0] === '' && this.parent === null) {
      path.shift();
      return this.findPath(path);
    } else if (path[0] === '..' && this.parent !== null) {
      path.shift();
      return this.parent.findPath(path);
    } else if (this.files.hasOwnProperty(path[0])) {
      let fname = path.shift();
      return this.files[fname].findPath(path);
    } else {
      throw new Error("Error: Path not found " + path.join('/'))
    }
  }
  pwd(list = []) {
    if (this.parent != null) {
      return this.parent.pwd([this.name].concat(list));
    } else return [this.name].concat(list);
  }
}

function require(src) {
  var new_script = document.createElement('script');
  new_script.setAttribute('src', src);
  document.head.appendChild(new_script);
  return new Promise((resolve) => {
    new_script.addEventListener('load', function() {
      resolve();
    });
  })
}