let app = {
  data() {
    return {
      db : null,
      ratings : [],
      sort_param: "Title",
      sort_asc: true,
      sort_params: {
        'Titre': 'Title',
        'Note': 'Your Rating',
        'Date de la note': 'Date Rated',
        'Durée': 'Runtime (mins)',
        'Sortie': 'Release Date'
      },
      search_field: "",
      limit: 32,
      step: 16,
      viewtype: 'list',
      observer: null,
    }
  },
  watch: {
    sort_param() {
      this.updateRatings();
    },
    sort_asc() {
      this.updateRatings();
    },
    search_field() {
      this.updateRatings();
    }
  },
  async created() {
    config = {
      locateFile: filename => `/js/dist/${filename}`
    }
    const sqlPromise = initSqlJs(config);
    const dataPromise = fetch("assets/db/ratings.sqlite").then(res => res.arrayBuffer());
    const [SQL, buf] = await Promise.all([sqlPromise, dataPromise])
    
    //Create the database
    this.db = new SQL.Database(new Uint8Array(buf));
    this.updateRatings();
    this.updateChart();
    let options = {
      threshold: [0.5],
      rootMargin: '100px'
    }
    
    this.observer = new IntersectionObserver(this.next, options);
    this.observer.observe(this.$refs.intersection)
  },
  methods: {
    next(a, b) {
      console.log(a, b)
      this.limit += this.step;
      this.updateRatings();
    },
    toObject(query) {
      return query.values.map((a) => {
        let b = {};
        query.columns.forEach((key, num) => {
          b[key] = a[num];
        })
        return b
      })
    },
    changeSort(param) {
      if (param === this.sort_param) {
        this.sort_asc = ! this.sort_asc;
      } else {
        this.sort_asc = true;
      }
      this.sort_param = param;
    },
    updateRatings() {
      this.ratings = [];

      let sql_query = "SELECT * FROM ratings WHERE Image IS NOT NULL ";
      if (this.search_field !== "")
        sql_query += `AND Title LIKE '%${this.search_field.replaceAll("'", "")}%' `;
      sql_query += `order by "${this.sort_param}" ${this.sort_asc ? 'ASC':'DESC'} LIMIT ${this.limit}`;

      let stmt = this.db.prepare(sql_query);

      while (stmt.step()) {
        let step = stmt.getAsObject();
        step.Genres = step.Genres.split(", ");
        this.ratings.push(step);
      }
      stmt.free();
    },
    updateChart() {

      let sql_query = `select substr("Date Rated", 1, 7) as date_rated, count("Const") as count, avg("Your Rating") as avg from ratings group by date_rated`
      let stmt = this.db.prepare(sql_query);
      let sdata = [];
      while (stmt.step()) {
        let step = stmt.getAsObject();
        sdata.push(step);
      }
      stmt.free();
      // Lines
      const line_data = {
        labels: sdata.map(a => a.date_rated),
        datasets: [{
            label: 'Moyenne',
            borderColor: 'rgb(255, 99, 132)',
            data: sdata.map(a => a.avg),
            yAxisID: 'y',
          },{
            label: 'Nombre',
            borderColor: 'rgb(1, 99, 132)',
            data: sdata.map(a => a.count),
            yAxisID: 'y1',
          },
        ]
      };
      new Chart(
        this.$refs.lines,
        {
          type: 'line',
          data: line_data,
          options: {
            responsive: true,
            interaction: {
              mode: 'index',
              intersect: false,
            },
            scales: {
              y: {
                type: 'linear',
                display: true,
                position: 'left',
              },
              y1: {
                type: 'linear',
                display: true,
                position: 'right',
              },
            }
          },
        }
      );

      // Radial
      sql_query = `select * from ratings`
      stmt = this.db.prepare(sql_query);
      let genres = new Set();
      sdata = [];
      while (stmt.step()) {
        let step = stmt.getAsObject();
        step.Genres = step.Genres.split(", ");
        for (let genre of step.Genres) {
          genres.add(genre);
        }
        sdata.push(step);
      }
      genres = [...genres]
      stmt.free();

      let radial_sources = genres.map(a => {
        let filtered = sdata.filter(b => b.Genres.indexOf(a) > -1)
        return { moy: filtered.reduce((c, d) => c + d["Your Rating"], 0) / filtered.length, med: filtered[Math.floor(filtered.length / 2)]["Your Rating"]}
      });
      
      const radial_data = {
        labels: genres,
        datasets: [{
          label: 'Moyenne par genre',
          borderColor: 'rgb(255, 99, 132)',
          data: radial_sources.map(a => a["moy"]),
        },{
          label: 'Médiane par genre',
          borderColor: 'rgb(1, 99, 132)',
          data: radial_sources.map(a => a["med"]),
        }
        ]
      };
      new Chart(
        this.$refs.radial,
        {
          type: 'radar',
          data: radial_data,
          options: {},
        }
      );
    },
  }
}


window.onload = () => {
  Vue.createApp(app).mount('#app')
}
