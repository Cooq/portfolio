
class Snake {
  constructor(vue_object, size=10, delta=500) {
    // Set variables for the game
    this.vue_object = vue_object;
    this.vue_object.interactive = false;
    this.size = size;
    this.game_window = [];
    this.directions = [[-1, 0], [0, -1], [1, 0], [0, 1]]
    this.movement = this.directions[0];
    this.last_movement = this.directions[0];
    this.message = "";

    // Init Game Window
    for(let i = 0; i < size; i++) {
      this.game_window.push([]);
      for(let j = 0; j < size; j++) {
        this.game_window[i].push(new Case());
      }
    }

    // Init Snake & food
    this.snake_position = [[1, 1], [1, 2], [1, 3], [1, 4]];
    this.snake_position.forEach(pos => this.get(pos).type = Case.SNAKE)
    this.addFood();

    // Set gaming loop and inputs
    this.interval = setInterval(() => this.loop(), delta);
    this.binded = this.input.bind(this);
    document.querySelector("body").addEventListener("keydown", this.binded)
  }

  // Main loop
  loop() {
    if (this.has_win()) return;
    // console.log(this.movement)
    // let inverse_movement = this.movement.map((a) => a * -1);
    // if(arrayEquals(inverse_movement, this.last_movement)) this.movement = this.last_movement;
    
    let head = this.snake_position[0].map(a => a);
    head[0] = (head[0] + this.size + this.movement[0]) % this.size;
    head[1] = (head[1] + this.size + this.movement[1]) % this.size;
    
    this.snake_position.unshift(head);

    switch (this.get(head).type) {
      case Case.FOOD:
        this.addFood();
        break;
      case Case.SNAKE:
      case Case.WALL:
        this.disconnect();
        this.message = "GAME OVER !";
      default:
        this.snake_position.pop();
        break;
    }

    this.forEach(c => { if (c.type == Case.SNAKE) {c.type = Case.VOID} })
    this.snake_position.forEach(pos => this.get(pos).type = Case.SNAKE)


    this.draw();
  }
  
  // Check inputs
  input(key) {
      key.preventDefault();
      if (key.keyCode >= 37 && key.keyCode <= 40) {
        this.movement = this.directions[key.keyCode - 37];
      }
  }

  // Draw the scene
  draw() {
    let res = this.game_window.reduce((a, b) => a + b.join("") + "\n", "");
    this.vue_object.writeLastLine(res);
    document.querySelector(".last_step").scrollIntoView({"block": "start"});
  }

  addFood() {
    let void_case = [...this.filter((a)=>a.type == Case.VOID)]
    void_case[Math.floor(Math.random()*void_case.length)].type = Case.FOOD;
  }

  has_win() {
    let void_case = [...this.filter((a)=>a.type == Case.VOID)];
    if (void_case.length == 0) {
      this.disconnect();
      this.message = "YOU WIN !"
      return true;
    }
    return false
  }

  disconnect() {
    clearInterval(this.interval);
    this.vue_object.interactive = true;
    document.querySelector("body").removeEventListener("keydown", this.binded)
  }

  // HELPERS
  // Get coord of game_window
  get(x, y) {
    if (typeof x == "object") {
      y = x[1];
      x = x[0];
    }
    return this.game_window[y][x];
  }

  // For each case
  forEach(f) {
    for(let i = 0; i < this.size; i++) {
      for(let j = 0; j < this.size; j++) {
        f(this.get(i, j));
      }
    }
  }

  // Filter case
  * filter(f) {
    for(let i = 0; i < this.size; i++) {
      for(let j = 0; j < this.size; j++) {
        let o = this.get(i, j); 
        if (f(o)) yield o
      }
    }
  }
}

class Case {
  static VOID  = 0;
  static WALL  = 1;
  static FOOD  = 2;
  static SNAKE = 3;
  static SNAKE_HEAD = 4;

  constructor(type=Case.VOID) {
    this.type = type;
  }
  
  toString() {
    switch(this.type) {
      case Case.WALL: return "|";
      case Case.SNAKE: return "X";
      case Case.FOOD: return "O";
      case Case.VOID:
      default:
        return "_";
    }
  }
}

function arrayEquals(a, b) {
  return Array.isArray(a) &&
    Array.isArray(b) &&
    a.length === b.length &&
    a.every((val, index) => val === b[index]);
}