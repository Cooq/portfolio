// import { createApp } from 'vue'
figlet.defaults({fontPath: "fonts/figlets"});

let app = {
  data() {
    let available_commands = new Commands();
    return {
      title:"",
      data: {},
      history: [],
      prompt: available_commands.prompt,
      steps: [],
      interactive: true,
      available_commands: available_commands,
      connexion_prompt: `
This website comes with ABSOLUTELY NO WARRANTY, to the extent permitted
by applicable law.
Last login: ${new Date(Math.random()*((new Date()).getTime())).toLocaleString()} from ${(Math.floor(Math.random() * 255) + 1)+"."+(Math.floor(Math.random() * 255))+"."+(Math.floor(Math.random() * 255))+"."+(Math.floor(Math.random() * 255))}

${available_commands.help()}`
    }
  },
  watch: {
    steps :{
      handler() {
        let next_step = this.steps.indexOf(false);
        if (next_step > -1) {
          this.$nextTick(() => this.next_step(next_step))
        }
      },
      deep: true
    }
  },
  created() {
    fetch("assets/json/portfolio.json")
      .then(b => b.json())
      .then(b => {
        this.data = b;
        figlet(b.title, 'Slant', (err, text) => {
          if (err) return console.dir(err);
          this.title = text;
          this.steps.push(false)
      });
        this.$refs.prompt.focus()
      })
  },
  methods: {
    async validate() {
      if(!this.steps.every((a) => a)) return
      this.$refs.prompt.removeAttribute("data-command");
      try {
        let command = this.$refs.prompt.innerText.trim().replace(/\n/g, '');
        console.log(command);
        await this.available_commands.execute(command, this.history, this);
        this.steps.push(false);
        this.$refs.prompt.innerHTML = "";
        this.$refs.prompt.setAttribute("data-prompt", "");
        this.$refs.prompt.removeAttribute("data-index");
        this.$refs.prompt.focus();
      } catch (error) {
        console.error(error);
        this.steps.push(true);
      }
    },
    up() {
      let current_index = this.$refs.prompt.getAttribute("data-index");
      if (current_index == null) {
        current_index = this.history.length;
        this.$refs.prompt.setAttribute("data-prompt", this.$refs.prompt.innerText.trim());
      }
      current_index--;
      if (current_index < 0) { current_index = 0; }
      this.$refs.prompt.setAttribute("data-index", current_index);
      this.$refs.prompt.innerText = this.history[current_index]["command"];
      this.setCarretPosition(this.$refs.prompt.innerText.length);
    },
    down() {
      let current_index = this.$refs.prompt.getAttribute("data-index");
      if (current_index == null) { return }
      current_index++;
      if (current_index >= this.history.length) { 
        this.$refs.prompt.removeAttribute("data-index");
        this.$refs.prompt.innerText = this.$refs.prompt.getAttribute("data-prompt");
      } else {
        this.$refs.prompt.setAttribute("data-index", current_index);
        this.$refs.prompt.innerText = this.history[current_index]["command"];
      }
      this.setCarretPosition(this.$refs.prompt.innerText.length)
    },
    next_step(step) {
      switch (step) {
        case 0:
          type(this, (t) => {this.title = t}, this.title, 0, () => this.steps.push(false));
          break;
        case 1:
          type(this, (t) => {this.connexion_prompt = t}, this.connexion_prompt, step, () => document.querySelector("#prompt").focus());
          break;
        default:
          if (this.history[step-2])
            type(this, (t) => {
              this.history[step-2]["result"] = t;
              document.querySelector(".last_step").scrollIntoView({"block": "start"})
            },
            this.history[step-2]["result"], step, () => document.querySelector("#prompt").focus());
      }
      // if (step === 1) {
      // } else if (step < this.steps.length) {
      // }
      this.$refs.prompt.focus();
    },
    clear(e) {
      this.history.forEach(c => c.clear = true);
    },
    removeAutoComplete(e) {
      if (e.key !== "Tab") {
        this.$refs.prompt.removeAttribute("data-pre-command");
      }
    },
    autoComplete() {
      let prefix_command = this.$refs.prompt.getAttribute("data-pre-command");
      let current_text = this.$refs.prompt.innerText;
      if (!prefix_command) {
        prefix_command = this.$refs.prompt.innerText;
        this.$refs.prompt.setAttribute("data-pre-command", prefix_command);
      }
      let new_command = this.available_commands.autoComplete(prefix_command, current_text, this);
      console.log(prefix_command, current_text, new_command)
      this.$refs.prompt.innerText = new_command;
      this.setCarretPosition(this.$refs.prompt.innerText.length);
      
    },
    setCarretPosition(n) {
      var tag = this.$refs.prompt;
      var setpos = document.createRange();
      var set = window.getSelection();
      setpos.setStart(tag.childNodes[0], n);
      setpos.collapse(true);
      set.removeAllRanges();
      set.addRange(setpos);
      tag.focus();
    },
    writeLastLine(text) {
      this.history[this.history.length-1]["result"] = text;
    }
  }
  
}

function type(vue_object, set_text, base_text, step_index, after = () => {}, current_text="", timeout=100, chunk_size=null, desired_time=2000) {
  if (!chunk_size) {
    chunk_size = Math.ceil(base_text.length / (desired_time / timeout))
    if (chunk_size < 100) chunk_size = 100;
  }
  let index = current_text.length;
  set_text(current_text);
  let next_index = index + chunk_size;
  if (next_index >= base_text.length) {
    set_text(base_text);
    vue_object.steps[step_index] = true;
    after();
  } else {
    setTimeout(() => {type(vue_object, set_text, base_text, step_index, after, current_text+base_text.slice(index, next_index))}, timeout);
  }

}


window.onload = () => {
  document.querySelector("html").addEventListener("click", () =>{
    document.querySelector("#prompt").focus()})
  Vue.createApp(app).mount('#app')
}
