class MoviesCommands {

  HELP_TEXT = `Usage:
  - movies list [--page PAGE] [--order COLUMN] [--asc|--desc] : List all movie I have seen
  - movies get [TITLE] [--page PAGE] [--order COLUMN] [--asc|--desc] : List all movie I have seen with TITLE with their rating`;

  COLUMNS = [
    "Title",
    "Const",
    "Your Rating",
    "Date Rated",
    "URL",
    "Title Type",
    "IMDb Rating",
    "Runtime (mins)",
    "Year",
    "Genres",
    "Num Votes",
    "Release Date",
    "Directors",
    "Image"
  ]

  constructor(vue_object) {
    this.vue_object = vue_object;
    this.limit = 16;
    let config = {
      locateFile: filename => `/js/dist/${filename}`
    }
    this.sqlPromise = initSqlJs(config);
    this.dataPromise = fetch("assets/db/ratings.sqlite").then(res => res.arrayBuffer());
    this.db = new Promise((resolve) => Promise
      .all([this.sqlPromise, this.dataPromise])
      .then(([SQL, buf]) => {
        let db = new SQL.Database(new Uint8Array(buf));
        this.n_ratings = this.runQuery("SELECT count(*) FROM ratings;", db)[0]["count(*)"]
        this.n_pages = Math.floor(this.n_ratings / this.limit);
        resolve(db);
    }));
  }

  runQuery(query, db) {
    console.debug(query);
    let stmt = db.prepare(query);
    let results = [];
    while (stmt.step()) {
      results.push(stmt.getAsObject());
    }
    stmt.free();
    return results;
  }

  async parseParameters(named_parameters) {
    let db = await this.db;
    let params = named_parameters["_"]; 
    if (params.length == 0) {
      return `${this.n_ratings} ratings found.`;
    } else if (params.length >= 1) {
      let offset = (parseInt(named_parameters["page"]) || 0) * this.limit;
      let order  = (named_parameters["order"]) || this.COLUMNS[0];
      if (!this.COLUMNS.includes(order)) return `Error: Order accepted are the following ${this.COLUMNS.reduce((p, a)=> p + "\n" + a)}`;
      let order_asc  = (named_parameters["asc"]) || !(named_parameters["desc"]);
      let order_query = `order by "${order}" ${order_asc? 'ASC': 'DESC'} limit ${this.limit} offset ${offset};`;
      let res;
      switch (params[0]) {
        case "list":
          res = this.runQuery(`SELECT * FROM ratings ${order_query}`, db);
          return res.reduce((p, rating) => `${p}${rating["Title"]} (${rating["Year"]})\n`, "") + `--- page ${offset / this.limit} / ${this.n_pages} ---`;
        case "get":
          let title = params[1] || null;
          let query = `SELECT * FROM ratings ${order_query}`
          if (title) {
            query = `SELECT * FROM ratings where UPPER(Title) like UPPER('%${title}%') ${order_query}`
          }
          res = this.runQuery(query, db);
          let text = res.reduce((p, rating) => `${p}<a href="${rating["URL"]}">${rating["Title"]}<img class="poster" loading="lazy" src="${rating['Image']}"> (${rating["Year"]})</a> <div class="rating" value="${rating['Your Rating']}"></div>\n`, "");
          text += `--- page ${offset / this.limit} / ${this.n_pages} ---`
          return text;
      }
    }
    return this.HELP_TEXT;
  }

  async run(arg, named_parameters) {
    let text = "There was an error..."
    try {
      text = await this.parseParameters(named_parameters);
    } catch (error) {
      console.error(error);
    }
    this.vue_object.writeLastLine(text);
    this.vue_object.interactive = true;
  }
}