#! /bin/bash

# Get new ratings
COOKIES=$1
IMDB_USER_ID=$2

curl "https://www.imdb.com/user/$IMDB_USER_ID/ratings/export" \
  -H 'authority: www.imdb.com' \
  -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  -H 'accept-language: fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7' \
  -H 'cache-control: no-cache' \
  -H "cookie: $COOKIES" \
  -H 'pragma: no-cache' \
  -H "referer: https://www.imdb.com/user/$IMDB_USER_ID/ratings?ref_=nv_usr_rt_4" \
  -H 'sec-ch-ua: " Not A;Brand";v="99", "Chromium";v="101", "Google Chrome";v="101"' \
  -H 'sec-ch-ua-mobile: ?0' \
  -H 'sec-ch-ua-platform: "Windows"' \
  -H 'sec-fetch-dest: document' \
  -H 'sec-fetch-mode: navigate' \
  -H 'sec-fetch-site: same-origin' \
  -H 'sec-fetch-user: ?1' \
  -H 'upgrade-insecure-requests: 1' \
  -H 'user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.54 Safari/537.36' \
  --compressed > ratings.csv

# Import new ratings in the database
sqlite3 static/assets/db/ratings.sqlite 'CREATE TABLE IF NOT EXISTS "ratings"( "Const" TEXT, "Your Rating" INTEGER, "Date Rated" DATE, "Title" TEXT, "URL" TEXT, "Title Type" TEXT, "IMDb Rating" REAL, "Runtime (mins)" REAL, "Year" INTEGER, "Genres" TEXT, "Num Votes" INTEGER, "Release Date" DATE, "Directors" TEXT, "Image" TEXT, PRIMARY KEY("Const"));'
sqlite3 static/assets/db/ratings.sqlite '.mode csv' '.import ratings.csv ratings'
sqlite3 static/assets/db/ratings.sqlite "delete from ratings where Const = 'Const'; "

rm ratings.csv

# Import poster from url
function getImage {
  script_regex='<script type="application\/ld\+json">([^<]+)<\/script>';
  [[ $1 =~ $script_regex ]]; # Match LDJSON Script

  script_val=${BASH_REMATCH[1]};
  image_regex='"image":"((\\"|[^"])+)"'
  [[ $script_val =~ $image_regex ]]; # Match URL from LDJSON

  image_url=${BASH_REMATCH[1]};
}

function doRequest {
  html_val="$(curl $1)"
}

function getRatings {
  ratings=$(sqlite3 static/assets/db/ratings.sqlite 'select URL, Const from ratings where image is null;')
  for i in ${ratings}
  do
    [[ $i =~ ([^\|]+)\|([^\|]+) ]]; # Match URL from LDJSON
    url=${BASH_REMATCH[1]}
    const=${BASH_REMATCH[2]}
    doRequest "$url"
    getImage "$html_val"
    sqlite3 static/assets/db/ratings.sqlite "update ratings set image = '$image_url' where const = '$const';"
  done
}

getRatings
